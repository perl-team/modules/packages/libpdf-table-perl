libpdf-table-perl (1:1.006-1) unstable; urgency=medium

  * Import upstream version 1.006.
  * d/copyright: bump upstream copyright year.
  * d/control: add myself to uploaders.
  * Declare compliance with Debian Policy 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Tue, 21 May 2024 19:45:32 +0200

libpdf-table-perl (1:1.005-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.005.
  * d/copyright: bump upstream copyright year.
  * Declare compliance with Debian Policy 4.6.2.
  * typo.patch: remove: applied upstream.
  * d/control: reconstruct PDF::Table.3pm from Table.pod.

 -- Étienne Mollier <emollier@debian.org>  Mon, 21 Aug 2023 22:06:02 +0200

libpdf-table-perl (1:1.003-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.003.
  * d/copyright: update years following new upstream version.
  * Declare compliance with Debian Policy 4.6.1.
  * Add typo.patch: fix typo caught by lintian.
  * d/s/lintian-overrides: flag false positive on INFO/Table.html.
  * d/rules: put back content in PDF::Table manual page.

 -- Étienne Mollier <emollier@debian.org>  Thu, 21 Jul 2022 19:51:34 +0200

libpdf-table-perl (1:1.002-1) unstable; urgency=medium

  * Import upstream version 1.002.
  * Declare compliance with Debian Policy 4.5.1.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Tue, 29 Dec 2020 21:17:03 +0100

libpdf-table-perl (1:0.12-1) unstable; urgency=medium

  * debian/watch: use uscan version 4.
  * Import upstream version 0.12.
  * Update debian/upstream/metadata.
  * debian/copyright: update Upstream-Contact, copyright holders and
    years.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Apr 2020 11:22:45 +0200

libpdf-table-perl (1:0.11.0-1) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Add debian/gbp.conf
  * Import upstream version 0.11.0

 -- Xavier Guimard <yadd@debian.org>  Sat, 17 Aug 2019 22:36:55 +0200

libpdf-table-perl (1:0.10.1-1) unstable; urgency=medium

  * Imported upstream version 0.10.1
  * Declare conformance with Policy 4.1.4 (no changes needed)
  * Remove spelling errors patch, now included in upstream

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 21 Apr 2018 08:20:14 +0200

libpdf-table-perl (1:0.10.0-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * New upstream release
  * declare conformance with Policy 4.1.3 (no changes needed)
  * Bump debhelper compatibility level to 10
  * Add patch for spelling errors (reported)
  * Add upstream/metadata

 -- Xavier Guimard <x.guimard@free.fr>  Thu, 08 Mar 2018 11:58:58 +0100

libpdf-table-perl (1:0.9.14-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Thomas Goirand ]
  * Team upload.
  * New upstream release (Closes: #887994).
  * Add an EPOCH, since upstream moved from 0.9.6003 to 0.9.14.
  * Added Testsuite: autopkgtest-pkg-perl.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Jan 2018 14:40:44 +0100

libpdf-table-perl (0.9.6003-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Update debian/watch to accept 0.9.6_h3 version and change it into 0.9.6003
  * Imported Upstream version 0.9.6003
  * Bump debian/copyright to 1.0
  * Bump Standards-Version to 3.9.4
  * Remove spelling patch now included in upstream

 -- Xavier Guimard <x.guimard@free.fr>  Thu, 04 Apr 2013 05:54:57 +0200

libpdf-table-perl (0.9.5-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Switch to source format 3.0 (quilt). Remove quilt framework.
  * Remove patch, applied upstream.
  * Use tiny debian/rules.
  * debian/copyright: update formatting.
  * Don't install README anymore.
  * Set Standards-Version to 3.9.2, remove version from perl build
    dependency.
  * Bump debhelper compatibility level to 8.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Thu, 20 Oct 2011 18:17:33 +0200

libpdf-table-perl (0.9.3-3) unstable; urgency=low

  * debian/control:
    - changed: switched Vcs-Browser field to ViewSVN
    - added: ${misc:Depends} to Depends: field
    - change my email address
    - mention module name in long description, remove trailing
      space and article from short description.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Add patch sample-module.patch: use the correct module name in the sample
    script; thanks to Gabor Kiss for the bug report (closes: #506408). Add
    quilt framework. Add debian/README.source.
  * Set Standards-Version to 3.8.0 (no changes).
  * debian/copyright: update formatting.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Dec 2008 23:56:35 +0100

libpdf-table-perl (0.9.3-2) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.
  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #467994).
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: add upstream URL, add year of copyright, convert to
    new format.
  * debian/rules: update based on dh-make-perl's templates.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 11 Mar 2008 23:02:09 +0100

libpdf-table-perl (0.9.3-1) unstable; urgency=low

  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 28 Feb 2007 14:08:08 -0600

libpdf-table-perl (0.9.2-1) unstable; urgency=low

  * New upstream release.
  * Set Standards-Version to 3.7.2 (no changes).
  * Set debhelper compatibility level to 5.
  * Add examples to /usr/share/doc/libpdf-table-perl/examples/.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 19 Nov 2006 19:13:32 +0100

libpdf-table-perl (0.02-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/watch.

 -- Niko Tyni <ntyni@iki.fi>  Tue, 18 Apr 2006 00:31:03 +0300

libpdf-table-perl (0.01-2) unstable; urgency=low

  * Add libpdf-api2-perl to Build-Depends-Indep. (Closes: #348560)
  * Add myself to Uploaders.

 -- Niko Tyni <ntyni@iki.fi>  Thu, 19 Jan 2006 20:15:19 +0200

libpdf-table-perl (0.01-1) unstable; urgency=low

  * Initial Release (Closes: #343275)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 13 Dec 2005 22:01:46 -0600
